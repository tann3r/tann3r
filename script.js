var domIsReady = (function(domIsReady) {
   var isBrowserIeOrNot = function() {
      return (!document.attachEvent || typeof document.attachEvent === "undefined" ? 'not-ie' : 'ie');
   }

   domIsReady = function(callback) {
      if(callback && typeof callback === 'function'){
         if(isBrowserIeOrNot() !== 'ie') {
            document.addEventListener("DOMContentLoaded", function() {
               return callback();
            });
         } else {
            document.attachEvent("onreadystatechange", function() {
               if(document.readyState === "complete") {
                  return callback();
               }
            });
         }
      } else {
         console.error('The callback is not a function!');
      }
   }

   return domIsReady;
})(domIsReady || {});

(function(document, window, domIsReady, undefined) {
   domIsReady(function() {
	   	var colours = ['93DFB8', 'FFC8BA', 'E3AAD6', 'B5D8EB', 'FFBDD8'];
	   	var picked = colours[Math.floor(Math.random()*colours.length)];
   		var nameh = document.getElementsByClassName('nameh')[0];
   		var main = document.getElementsByClassName('main')[0];
   		var links = document.getElementsByClassName('links')[0].getElementsByTagName('a');

         nameh.classList.remove('no-js');
         main.classList.remove('no-js');
         document.getElementsByClassName('links')[0].classList.remove('no-js');

   		nameh.className += ' c' + picked;
         main.className += ' b' + picked;

   		for (var i = 0; i < links.length; i++) {
   			links[i].className += ' h' + picked;
   		}

   });
})(document, window, domIsReady);
